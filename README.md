This is the webapp module for the distributor thin client. 
This will do following things
+ Allow distributors to process order.
+ Manager Sales employees
+ Route Delivery in fastest possible way
+ Keep a track on outstanding at a retailer.


The module has two components server & client, both mutually exclusive with respect to their requirements and configs...

How to build server. 

PreReq - Have coffeescript installed.

Building the code -
cd into server and type 'cake build' - This will create js variants of coffee code in dist folder.

Launch Server -
cake run - this will run the server as a forever process
to check type - 'forever list' in terminal

In case you wan to run the server as a terminal provess - cd dist && node server.js