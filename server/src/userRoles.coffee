'use strict'

_ = require 'lodash'

config =
  roles : [
    'public'
    'user'
    'admin'
  ]
  accessLevels :
    'public' : ['public', 'user', 'admin']
    'anon'   : ['public']
    'user'   : ['user', 'admin']
    'admin'  : ['admin']

buildRoles = (roles) ->
  _.reduce roles, (memo, role, index) ->
    memo[role] =
      bitMask: Math.pow 2, index
      title: role
    memo
  , {}

buildAccessLevels = (levelDecls, userRoles) ->
  _.reduce levelDecls, (memo, level, key) ->
    resultBitMask = _.reduce level, (memo, role) ->
      memo = memo | userRoles[role].bitMask
    , 0      
    memo[key] =
      bitMask: resultBitMask
      title: key
    memo
  , {}
        
userRoles    = buildRoles config.roles
accessLevels = buildAccessLevels config.accessLevels, userRoles

module.exports = {userRoles, accessLevels}
