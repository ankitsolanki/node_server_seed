# Copyright (C) 2016 Ankit Solanki <me@ankit.tech>

'use strict'

userRoles = require('../userRoles').userRoles
_         = require 'lodash'

module.exports = (req, res) ->
  role = req.user?.role || userRoles.public
  group = req.user?.group || 'public'
  if _.isString role then role = userRoles[role]
  userid = req.user?.userid || ''
  body = "__iiActiveUser="
  body += JSON.stringify {userid, role, group}
  res.setHeader 'Content-Type', 'text/javascript'
  res.setHeader 'Content-Length', body.length
  res.end body