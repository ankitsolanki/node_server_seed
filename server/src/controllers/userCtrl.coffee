# Copyright (C) 2016 Ankit Solanki <me@ankit.tech>
###
Module dependencies.
###
mongoose = require("mongoose")
User = mongoose.model("User")
_ = require 'lodash'

###
Auth callback
###
exports.authCallback = (req, res, next) ->
  res.redirect "/"

###
Show login form
###
exports.signin = (req, res) ->
  res.render "src/app/auth/login.tpl",
    title: "Signin"
    message: req.flash("error")

###
Show sign up form
###
exports.signup = (req, res) ->
  res.render "users/signup",
    title: "Sign up"
    user: new User()

###
Logout
###
exports.signout = (req, res) ->
  req.logout()
  res.redirect "/"

###
Session
###
exports.session = (req, res) ->
  res.redirect "/"

###
Create user
###
exports.create = (req, res) ->
  rObject = req.body
  rObject.provider = -1
  user = new User(rObject )
  user.provider = "local"

  User.findOne(email : rObject.email).exec (err, found_user) ->
    if err then return res.send 500, {"error" : err.toString()}
    if found_user 
      res.send 500, {"error" : "user with email id exist."}
    else
      user.save (err) ->
        if err then return res.send 500, {"error" : err.toString()}
        # This will login the user directly after him signing up.
        req.logIn user, (err) ->
          return res.status(500).send({error :err}) if err
          res.status(200).send(req.user)

###
Create user
###
exports.update = (req, res) ->
  rObject = req.body
  rObject.provider = -1
  user = new User(rObject )

  User.findOne(email : rObject.email).exec (err, found_user) ->
    if err then return res.send 500, {"error" : err.toString()}
    if found_user is undefined or found_user is null
      res.send 500, {"error" : "user with email id doesnt exist."}
    else
      found_user = _.assign found_user, rObject
      user = new User(found_user)
      user.save (err) ->
        if err then return res.send 500, {"error" : err.toString()}
        # This will login the user directly after him signing up.
        req.logIn user, (err) ->
          return res.status(500).send({error :err}) if err
          res.status(200).send(req.user)


###
Show profile
###
exports.show = (req, res) ->
  user = req.profile
  res.render "users/show",
    title: user.name
    user: user

###
Send User
###
exports.me = (req, res) ->
  res.jsonp req.user or null


###
Find user by id
###
exports.user = (req, res, next, id) ->
  User.findOne(_id: id).exec (err, user) ->
    return next(err)  if err
    return next(new Error("Failed to load User " + id))  unless user
    req.profile = user
    next()
