# Copyright (C) 2016 Ankit Solanki <me@ankit.tech>

'use strict'

_            = require 'underscore'
path         = require 'path'
passport     = require 'passport'
loginConfig  = require './userRoles'

# Controllers
defaultCtrl            = require './controllers/defaultCtrl'
userStatusCtrl         = require './controllers/userStatusCtrl'
userCtrl = require './controllers/userCtrl'

accessLevels = loginConfig.accessLevels
userRoles    = loginConfig.userRoles

routes = [
  {
    path: '/*'
    httpMethod: 'GET'
    middleware: [defaultCtrl]
  },
  {
    path: '/service/user/add'
    httpMethod: 'POST'
    middleware: [userCtrl.create]
  },
  {
    path: '/service/user/update'
    httpMethod: 'POST'
    middleware: [userCtrl.update]
  }
]

ensureAuthorized = (req, res, next) ->
  role = req.user?.role || userRoles.public
  if _.isString role then role = userRoles[role]
  accessLevel = _.findWhere(routes, { path: req.route.path }).accessLevel || accessLevels.public
  if not (accessLevel.bitMask & role.bitMask) then return res.send 401
  next()

module.exports = (app, passport) ->
  #app.post "/users/session/login", passport.authenticate("local",{failureRedirect: "/src/app/auth/login.tpl.html",failureFlash: "Invalid email or password."}), userCtrl.session

  app.post "/users/session/login", passport.authenticate('local', failWithError: true), ((req, res, next) ->
    return res.json(id: req.user.id)),
  (err, req, res, next) ->
    return res.status(401).send({error : err})
  # Making from routes..
  _.each routes, (route) ->
    route.middleware.unshift ensureAuthorized
    args = _.flatten [route.path, route.middleware]
    method_name = route.httpMethod.toLowerCase() || 'get'
    app[method_name]?.apply app, args
    

#module.exports = (app, passport) ->
#  contacts = require "../app/controllers/contactListcontroller"
#  # articles = require("../app/article/article-controller")
#  index = require("../app/controllers/index")
#
#  # User Routes
#  app.get "/signin", users.signin
#  app.get "/signup", users.signup
#  app.get "/signout", users.signout
#
#  # Users API
#  app.post "/users", users.create
#  app.post "/users/session", passport.authenticate("local",
#    failureRedirect: "/signin"
#    failureFlash: "Invalid email or password."
#  ), users.session
#  app.get "/users/me", users.me
#  app.get "/users/:userId", users.show
#  app.param "userId", users.user
#
#  # Article Routes
#  # app.get "/articles", articles.all
#  # app.post "/articles", passport.auth.requiresLogin, articles.create
#  app.post "/contacts/create", passport.auth.requiresLogin, contacts.saveContactList
#  app.get "/contacts/get", passport.auth.requiresLogin, contacts.getContactLists
#  app.delete "/contacts/delete", passport.auth.requiresLogin, contacts.deleteContactList
#
#
#  # Home route
#  app.get "/", index.render
