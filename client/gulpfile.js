//Task runner configurations.
var gulp = require('gulp'),
  buildConfig = require('./config/build.config'),
  gutil = require('gulp-util'),
  concat = require('gulp-concat'),
  argv = require('minimist')(process.argv.slice(2)),
  footer = require('gulp-footer'),
  header = require('gulp-header'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  karma = require('karma').server,
  cssmin = require('gulp-cssmin'),
  //karmaConf = require('./config/karma.conf.js'),
  sass = require('gulp-sass'),
  watch = require('gulp-watch');

gulp.task('build', function () {
  return gulp.src(buildConfig.jsFiles)
    .pipe(concat(buildConfig.jsFilename))
    .pipe(header(buildConfig.closureStart))
    .pipe(footer(buildConfig.closureEnd))
    .pipe(header(buildConfig.banner))
    .pipe(gulp.dest(buildConfig.dist.js))
    .pipe(uglify())
    .pipe(rename({
      extname: '.min.js'
    }))
    .pipe(gulp.dest(buildConfig.dist.js));
});

gulp.task('sass', function () {
  gulp.src(buildConfig.scssFiles)
    .pipe(sass())
    .pipe(gulp.dest(buildConfig.dist.css))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(buildConfig.dist.css));
});

gulp.task('watch', ['build', 'sass'], function () {
  gulp.watch([buildConfig.jsFiles[0], buildConfig.scssFiles[0]], ['build', 'sass']);
});

gulp.task('default', ['build', 'sass']);
