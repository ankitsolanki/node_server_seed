var fs = require('fs');
var pkg = require('../package.json');

module.exports = {
  jsFilename: 'app.js',
  cssFilename: 'app.css',
  banner:
    '/*!\n' +
    ' * See LICENSE in this repository for license information\n' +
    ' */\n',
  closureStart: '(function(){\n',
  closureEnd: '\n})();',

  dist: {js:'client/dist/js',css:'client/dist/css'},

  jsFiles: ['client/src/js/**/*.js'],
  scssFiles: ['client/src/scss/**/*.scss'],

  testFiles: ['src/test/**/*.js'],

  versionData: {
    version: pkg.version
  }
};

