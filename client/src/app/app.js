var App = angular.module("App", [
    "ui.router"
    ,"ui.bootstrap" 
    ,"oc.lazyLoad"
    ,"ngSanitize"
    ,"app.constants"
    ,"app.services"
    ,'validation.match'
    ,'ui.bootstrap.showErrors'
]); 

App.run(['$rootScope','$state','AuthService','AUTH_EVENTS',function($rootScope,$state,AuthService,AUTH_EVENTS){
    
    $rootScope.$on('$stateChangeStart',function(event,next,nextParams,fromState){
         if ('data' in next && 'authorizedRoles' in next.data) {
              var authorizedRoles = next.data.authorizedRoles;
             if (!AuthService.isAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    $state.go($state.current, {}, {reload: true});
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
             }
         }
        if (!AuthService.isAuthenticated()) {
            if (!((next.name === 'login')|| (next.name === 'signup') || next.name === 'forgot')) {
                event.preventDefault();
                $state.go('login');
            }
        }
    });
    
}]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
App.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
App.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
App.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: 'vendor',
        globalPath: 'vendor/global',
        layoutPath: 'vendor/layouts/layout',
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
App.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
App.controller('HeaderController', ['$scope','AuthService','$state', function($scope,AuthService,$state) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });

    // $scope.logout = function(){
    //     AuthService.logout(); 
    // };

    $scope.logout = function(data){
        AuthService
        .logout()
        .then(function(result){
             $state.go('login', {}, {reload: true});
        });
    };
    $scope.username = AuthService.username();
}]);

/* Setup Layout Part - Sidebar */
App.controller('SidebarController', ['$state', '$scope', function($state, $scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar($state); // init sidebar
    });
}]);




/* Setup Layout Part - Footer */
App.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
App.config(['$stateProvider', '$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider,$locationProvider) {
    // Redirect any unmatched url
    //$urlRouterProvider.otherwise("/dashboard");  
    $urlRouterProvider.otherwise(function($injector,$location){
        var $state = $injector.get('$state');
        $state.go('dashboard');
    });
    
    $stateProvider
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "src/app/auth/dashboard.tpl.html",            
            data: {pageTitle: 'Admin Dashboard Template'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'App',
                        insertBefore: '#ng_load_plugins_before', 
                        // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'assets/scripts/dashboard.js'
                        ] 
                    });
                }]
            }
        })
        .state('login', {
            url: "/login",
            templateUrl: "src/app/auth/login.tpl.html",            
            data: {pageTitle: 'Login Template'},
            controller: 'LoginCtrl'
        })
        .state('signup', {
            url: "/signup",
            templateUrl: "src/app/auth/signup.tpl.html",            
            data: {pageTitle: 'Signup Template'},
            controller: 'LoginCtrl'
        })
        .state('forgot', {
            url: "/forgot",
            templateUrl: "src/app/auth/forgot_password.tpl.html",            
            data: {pageTitle: 'Forgot Password Template'},
            controller: 'LoginCtrl'
        })
}]);

/* Init global settings and run the app */
App.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);

App.controller('LoginCtrl', function($rootScope, $scope, $http, $timeout,AuthService,$state) {
    $scope.data = {};
    $scope.invlidFormClass="display-hide";
    $scope.login = function(data){
        // AuthService
        // .login(data.username,data.password)
        // .then(function(authenticated){
        //      $state.go('dashboard', {}, {reload: true});
        //      $scope.setCurrentUsername(data.username);
        // },function(err){
        //         //show alert..
        //         console.log("Login failed..");
          //          $scope.invlidFormClass="";
        // })
        // ;
    };


    $scope.signup = function(data){
        if($scope.singupForm.$valid){
            console.log("Form is valid..");
              /*  AuthService.signup(data.username,data.email,data.password)
                    .then(function(authenticated){
                        $state.go('dashboard', {}, {reload: true});
                        $scope.setCurrentUsername(data.username);
                    },function(err){
                         //show alert..
                        console.log("Signup failed..");
                    }); */
        }else{
            console.log('form data is invalid');
        }
    };
     $scope.setCurrentUsername = function(name) {
        $scope.username = name;
    };

});

App.controller('DashboardController', function($rootScope, $scope, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});